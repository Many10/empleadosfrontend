import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const employeeModule = () => import('./employee/employee.module').then(mod => mod.EmployeeModule);

const routes: Routes = [
  {
    path: 'app',
    children: [
      {
        path: 'employee',
        loadChildren: employeeModule
      }
    ]
},
{
  path: '',
  redirectTo: '/app/employee/index',
  pathMatch: 'full'
},
{
  path: '**',
  redirectTo: '/app/employee/index',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
