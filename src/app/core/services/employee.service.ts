import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Employee } from '../model/employee.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends ServiceService<Employee> {

  constructor(http: HttpClient) {
    super();
    this.httpClient = http;
    this.resource = 'Employee/';
   }
}
