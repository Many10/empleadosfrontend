import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService<T> {

  public httpClient: HttpClient;
  public apiUrl: string;
  public resource: string;

  constructor() {
    this.apiUrl = environment.api;
  }

  public get(id: string): Observable<T> {
    return this.httpClient.get(this.getFullPath() + id).pipe(
        map((res: any) => res)
      );
  }

  getAll(path: string): Observable<Array<T>> {
    return this.httpClient.get(this.getFullPath() + path).pipe(
      map((res: any) => res)
      );
  }

  public getParams(parameters: HttpParams): Observable<T> {
    return this.httpClient.get(this.getFullPath(), {  params: parameters }).pipe(
        map((res: any) => res)
      );
  }

  public save(data: any): Observable<number> {
    return this.httpClient.post(this.getFullPath(), data).pipe(
        map((res: any) => res)
      );
  }

  public update(id: string, data: any): Observable<string> {
    return this.httpClient.put(this.getFullPath() + id, data).pipe(
        map((res: any) => res)
      );
  }

  public delete(id: string): Observable<string> {
    return this.httpClient.delete(this.getFullPath() + id).pipe(
        map((res: any) => res)
      );
  }

  public post(data: any): Observable<any> {
    return this.httpClient.post(this.getFullPath(), data).pipe(
        map((res: any) => res)
      );
  }
  public executeGet(path: string): Observable<any> {
    return this.httpClient.get(this.getFullPath() + path).pipe(
      map((res: any) => res)
    );
  }

  public executePost(path: string, body: any): Observable<any> {
    if (!path) {
      path = '';
    }
    return this.httpClient.post(this.getFullPath() + path, body).pipe(
      map((res: any) => res)
    );
  }

  public executeDelete(path: string): Observable<any> {
    return this.httpClient.delete(this.getFullPath() + path).pipe(
        map((res: any) => res)
      );
  }

  protected getFullPath() {
    return this.apiUrl + this.resource;
  }

}
