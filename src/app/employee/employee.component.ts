import { Component, OnInit } from '@angular/core';
import { Employee } from '../core/model/employee.model';
import { EmployeeService } from '../core/services/employee.service';
import { finalize } from 'rxjs/operators';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employees: Array<Employee> = [];
  public error: string;
  public loading = false;
  public formGroup: FormGroup;

  constructor(private employeeService: EmployeeService) { 
    this.formGroup = new FormGroup({
        employeeId: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.getEmployeesList();
  }

  public getEmployeesList() {
    this.loading = true;
    this.employeeService.getAll("getEmployees").pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (employees: Array<Employee>) => {
        if (employees != null) {
          this.employees = employees;
        }
      },
      error => {
        this.employees = [];
        this.error = error.error;
      }
    );
  }

  public search() {
    this.loading = true;
    const id = this.formGroup.get("employeeId").value;
    if (id === '') {
      this.getEmployeesList();
    } else {
      this.employeeService.get("getEmployee/" + id).pipe(
        finalize(
          () => this.loading = false
        )
      ).subscribe(
        (employee: Employee) => {
          if (employee != null) {
            this.employees = [];
            this.employees.push(employee);
          } 
        },
        error => {
          this.getEmployeesList();
          this.error = error.error;
        }
      );
    }
  }

}
